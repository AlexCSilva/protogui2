
#ifndef _PROTOGUI_H
#define _PROTOGUI_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "windows.h"

static const char* PROTOGUI_VERSION = "0.1.0";

class Proto;
typedef void (*callback_func)(Proto*);
typedef struct CONTROL {
	int id;
	HWND handle;
	callback_func func;
};

class Proto
{

private:

	const int CONTROL_OFFSET = 10000;
	int pEvent;
	std::vector<CONTROL> pControls;
	std::vector<CONTROL> pWindows; // TODO: I don't remember where I was going with this
	HFONT pFont; // the default font
	HWND pHwndEvent;
	HWND pWindowId;

public:

	Proto()
	{

		pEvent = NULL;
		pHwndEvent = NULL;
		pFont = CreateFont(14, 0, 0, 0, FW_REGULAR, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, DEFAULT_PITCH, "Microsoft Sans Serif");


	};

	~Proto()
	{

		pControls.clear();

	};

	void open(std::string title, int x, int y, int w, int h)
	{

		std::ostringstream stream;
		stream << (void const*) this;
		std::string windowClass = stream.str(); //  use ptr as unique id

		WNDCLASSEX wndclass;
		wndclass.cbSize = sizeof(WNDCLASSEX);
		wndclass.style = CS_HREDRAW | CS_VREDRAW;
		wndclass.lpfnWndProc = wndproc;
		wndclass.cbClsExtra = 0;
		wndclass.cbWndExtra = 0;
		wndclass.hInstance = GetModuleHandle(0);//hInstance;
		wndclass.hIcon = LoadIcon (NULL, IDI_APPLICATION);
		wndclass.hCursor = LoadCursor (NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH) COLOR_WINDOW;
		wndclass.lpszMenuName = NULL;
		wndclass.lpszClassName = windowClass.c_str();
		wndclass.hIconSm = NULL;
		RegisterClassEx(&wndclass);

		pWindowId = CreateWindowEx(NULL, windowClass.c_str(), title.c_str(), WS_TILEDWINDOW, x, y, w, h, NULL, NULL, NULL, NULL);

		std::string d = "protog2";
		SetProp(pWindowId, d.c_str(), this ); //reference this object for wndproc

	};

	void close()
	{

		DestroyWindow(pWindowId);

	};

	void show()
	{

		ShowWindow(pWindowId, SW_SHOWDEFAULT);
		UpdateWindow(pWindowId);

	};

	void hide()
	{

		ShowWindow(pWindowId, SW_HIDE);
		UpdateWindow(pWindowId);

	};

	int addLabel(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		CONTROL t;
		t.func = (callback_func) callback;
		t.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created LABEL with id: " << t.id <<"\n";
		t.handle = CreateWindowEx(STYLEX, "STATIC", text.c_str(), STYLE | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) t.id, NULL, NULL);
		pControls.push_back(t);
		SendMessage(t.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return t.id;

	}

	int addButton(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		CONTROL n;
		n.func = (callback_func) callback;
		n.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created BUTTON with id: " << n.id <<"\n";
		n.handle = CreateWindowEx(STYLEX, "BUTTON", text.c_str(), STYLE | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) n.id, NULL, this);
		pControls.push_back(n);
		return n.id;

	}

	int addInput(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		int style_ = ES_AUTOHSCROLL | STYLE;
		int stylex_ = WS_EX_CLIENTEDGE | STYLEX;

		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created INPUT with id: " << c.id <<"\n";
		c.handle = CreateWindowEx(stylex_, "EDIT", text.c_str(), style_ | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);

		SetWindowText(c.handle, text.c_str());
		pControls.push_back(c);
		return c.id;

	}

	int addEdit(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		int style_ = WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | STYLE;
		int stylex_ = WS_EX_CLIENTEDGE | STYLEX;
		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created EDIT with id: " << c.id <<"\n";

		c.handle = CreateWindowEx(stylex_, "EDIT", text.c_str(), style_ | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);
		SetWindowText(c.handle, text.c_str());

		pControls.push_back(c);
		return c.id;

	}

	int addGroup(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created GROUP with id: " << c.id <<"\n";

		c.handle = CreateWindowEx(STYLEX, "BUTTON", text.c_str(), STYLE | WS_VISIBLE | WS_CHILD | BS_GROUPBOX, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);
		SetWindowText(c.handle, text.c_str());

		pControls.push_back(c);
		return c.id;

	}

	std::string getString(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return t;

	}

	int getInt(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stoi(t);

	}

	float getFloat(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stof(t);

	}

	double getDouble(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stod(t);

	}

	long getLong(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stoi(t);

	}

	int getEvent()
	{

		poll();
		int r = pEvent;
		if (r != 0)
		{

			std::cout << this << ": received event from " << pEvent << "\n";
			pEvent = NULL;

		}

		return r;

	}

private:

	bool poll()
	{

		MSG msg;

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{

			TranslateMessage(&msg);
			DispatchMessage(&msg);
			return true;

		}

		return false;

	}

	void setEvent(int n, HWND h)
	{

		pEvent = n;
		pHwndEvent = h; // TODO: this is the the controls hwnd, not the window the control is attached to

	}

	HWND getHandle(int id)
	{

		for (std::vector<CONTROL>::iterator it = pControls.begin() ; it != pControls.end(); ++it)
		{

			if (it->id == id)
			{

				return it->handle;

			}

		}

		return 0;

	}

	static LRESULT CALLBACK wndproc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{

		Proto* t = (Proto*) GetProp(hWnd, "protog2"); // retrieve the pointer address that had the event. TODO: probably a bug because hWnd is not the necessarily the window
		t->protoWndProc(hWnd, uMsg, wParam, lParam); // call the objects version in it's place
		return DefWindowProc(hWnd, uMsg, wParam, lParam);

	};

	void protoWndProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
	{

		switch(umsg)
		{

			case WM_COMMAND:

				for (std::vector<CONTROL>::iterator it = pControls.begin() ; it != pControls.end(); ++it)
				{
					if (it->id == wparam)
					{
						if (it->func != NULL) // check for a callback pointer and do stuff
						{
							it->func(this);
						}
						setEvent(wparam, hwnd);
						break;
					}
				}

				break;

			case WM_SIZE: // TODO:
				break;

			case WM_SIZING: // TODO:
				break;

			case WM_CLOSE: // TODO:
				break;

		}

};

};

#endif // _PROTOGUI_H
