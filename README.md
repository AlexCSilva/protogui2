
```
#!c++

#include <iostream>
#include <thread>
#include <chrono>

#include "protogui.h"

void elClick(Proto* i)
{
	std::cout << "This is a callback\n";
}

int main()
{

	Proto ui = Proto();
	ui.open("Hello World", 0, 0, 250, 200);
	int button = ui.addButton("Test", 5, 105, 225, 50, (void*) elClick);
	ui.addLabel("If you can read this then good news, ProtoGUI didn't crash! However... you are now using ProtoGUI", 5, 5, 225, 50);
	int input = ui.addInput("42", 5, 80, 225, 20);
	ui.show();

	while(true)
	{

		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if (int e = ui.getEvent())
		{
			if (e == button)
			{
				std::cout << "Input integer: " << ui.getInt(input) << "\n";
			}
		}

	}

	return 1;

}
```
This is an unfinished prototyping library for projects that need a UI. It is very simple, very small and probably very broken.

**Please don't use this for super serious projects.** I didn't follow any standard or model when I wrote this and I am positive someone who knows more than about gui development would vomit when they see this. I only wanted a very lightweight and easily managed ui library.

You will probably need to link libcomctl32 and use the -lgdi32 option.
I have only compiled and tested it with mingw and friends on Windows 7 and Windows 10 using visual studio community edition.

X11 support soon, hopefully.