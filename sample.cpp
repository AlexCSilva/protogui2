#include <iostream>
#include <thread>
#include <chrono>

#include "protogui.h"

void elClick(Proto* i)
{
	std::cout << "This is a callback\n";
}

int main()
{

	Proto ui = Proto();
	ui.open("Hello World", 0, 0, 250, 200);
	int button = ui.addButton("Test", 5, 105, 225, 50, (void*) elClick);
	ui.addLabel("If you can read this then good news, ProtoGUI didn't crash! However... you are now using ProtoGUI", 5, 5, 225, 50);
	int input = ui.addInput("42", 5, 80, 225, 20);
	ui.show();

	while(true)
	{

		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if (int e = ui.getEvent())
		{
			if (e == button)
			{
				std::cout << "Input integer: " << ui.getInt(input) << "\n";
			}
		}

	}

	return 1;

}
